IOTA Crypto Core - release 0.24rv


0.24rv
------

- imp: supports now SPI additionally to UART as command interface

0.23rv
------

- imp: reverted last change on jsonDataTX (now returns tryte array again like in 0.21rv)

0.22rv
------

- imp: fast mbug-mini-pow
- imp: rewrote partial bundle.cpp - removed legacy code
- imp: tag jsonDataTX and prepareTransfers optional and variable length
- imp: pow now optional jsonDataTX
- fix: secureSeed and secureSeedBIP39; same method name but different signature made problems during debugging

0.21rv
------

- fix: missing define for json-library that lead to validation error of uint64_t data larger than 32bit

0.20rv
------

- imp: rewrote a part of hardware-testing for smaller memory requirements

0.19rv
------

- imp: changes on SBI interface
- imp: changed auth-strings to hex
- fix: signatureMessageFragments too long when calling signBundleHash

0.18rv
------

- imp: started wallet implementation and consolidated SBI-calls
- imp: more robust parameter validation when calling json api
- imp: enabled code-compression (saves 20-25% rom space)
- imp: separated API in different files
- imp: slot 7 now (always) is BIP39 (added part of trezor-crypto for BIP39)
- fix: signing in iota-lib didn't work

0.17rv
------
- imp: updated arduinojson from 5.x to 6.x
- imp: added bip39 seed variants
- imp: added trinity/nano-ledger -compatible api calls

0.16rv
------
- imp: mcycle implemented (vexriscv update) and delay_us for secure element improved (faster)

0.15rv
------
- fix: json parameter checks fixed
- fix: PoW fixed for TX that already had data in nonce
- imp: generateAddresses improved
- imp: signTransaction Api-call renamed to signBundleHash
- imp: limits api command added; it gives maximum number of tranactions / addresses
- fix: validators fixed for zero-pointer

0.14rv
------
- fix: added zero termination on signature fragments

0.13rv
------
- imp: moved big-to-little-endian conversion of sha512 to core

0.12rv
------
- imp: sha512 hardware acceleration

0.11rv
------
- imp: changes needed for core-update (converter v2)

0.10rv
------
- fix: proper timestamp for jsonDataTX

0.09rv
------
- fix: fixed bugs in generateAddress (always the same)
- fix: fixed bugs in attachToTangle (broken because of PMP)
- fix: fixed bugs in jsonDataTX (broken because of PMP)

0.08rv
------
- imp: addresses are generated and stored with CRC32

nt8_t	scratch[MAX_NUM_TRANSACTIONS * sizeof(TX_t)];
